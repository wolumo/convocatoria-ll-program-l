
package POJO;


public class Nota {
    private int idNota  ; 
    private String nombreAsignatura;
    private int calificacion; 

    public Nota(int idNota, String nombreAsignatura, int calificacion) {
        this.idNota = idNota;
        this.nombreAsignatura = nombreAsignatura;
        this.calificacion = calificacion;
    }

    public int getId() {
        return idNota;
    }

    public void setId(int id) {
        this.idNota = id;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }
    
}
