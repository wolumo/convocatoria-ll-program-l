
package POJO;


public class Estudiante {
private int id ;
private String carnet;
private String apellido;
private String cedula;
private String telefono;
private String nombres;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public Estudiante(int id, String nombres , String carnet, String apellido, String cedula, String telefono) {
        this.id = id;
        this.carnet = carnet;
        this.apellido = apellido;
        this.cedula = cedula;
        this.telefono = telefono;
        this.nombres=nombres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    

}
