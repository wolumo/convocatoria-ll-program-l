package DAO;
import java.io.IOException;
import java.util.*;
public interface IDao<T> {
  void create() throws IOException;
  int update() throws IOException;
  boolean delete () throws IOException;
  List<T>findAll ( ) throws IOException;
}