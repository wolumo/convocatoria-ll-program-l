
package DAO;
import POJO.Estudiante;
import java.util.List;
public interface IEstudiante extends IDao<Estudiante> {
    Estudiante findByCarnet();
    List<Estudiante> ListarTodos();
}
