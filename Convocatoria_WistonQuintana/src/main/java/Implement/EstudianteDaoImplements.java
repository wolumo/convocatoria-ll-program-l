package Implement;

import DAO.IEstudiante;
import POJO.Estudiante;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import POJO.Nota;

public class EstudianteDaoImplements implements IEstudiante {
     List<Estudiante> estudiantes = new ArrayList();
     private Scanner scan;
     List <Nota> notas = new ArrayList();

    @Override
    public Estudiante findByCarnet() {
        String carnet;
        System.out.println("Por favor digite el carnet ");
        carnet = scan.next();
        for ( int i = 0 ; i <estudiantes.size() ; i++){
            if(estudiantes.get(i).getCarnet().equals(carnet)){
               System.out.println ( "EL nombre es : " + estudiantes.get(i).getNombres() + "  y su  apellido" + estudiantes.get(i).            getApellido() + "con carnet : " + estudiantes.get(i).getCarnet()) ;
            }
        }
         return null;
    }

    @Override
    public List<Estudiante> ListarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create() throws IOException {
         int id , idNota , calificacion;
       String carnet , nombres , apellidos , cedula , telefono , nombreAsignatura, aumentador ;
        System.out.println("id : ");
        id = scan.nextInt();
        System.out.println("Carnet ");
        carnet = scan.next();
        System.out.println("Nombres");
        nombres = scan.next();
        System.out.println(" apellido : ");
        apellidos = scan.next();
        System.out.println("Cedula :");
        cedula = scan.next();
        System.out.println("telefono");
        telefono = scan.next();
        Estudiante e = new Estudiante (id ,nombres,  carnet , apellidos ,cedula , telefono  );
        estudiantes.add(e);
        System.out.println("Digite las notas :");
         aumentador = null;
        do{
            System.out.println("id de la nota :");
            idNota = scan.nextInt();
            System.out.println("Nombre de la asignatura :");
            nombreAsignatura = scan.next();
            System.out.println("Digite la calificacion : ");
            calificacion = scan.nextInt();
            Nota t = new Nota (idNota , nombreAsignatura , calificacion);
            notas.add(t);
            System.out.println("Desea seguir metiendo notas? :");
            aumentador = scan.next();
        }while(aumentador.equals("si"));
        
        System.out.println("El estudiante se ha guardado satisfactoriamente");
         }

    @Override
    public int update() throws IOException {
        int id ;
        System.out.println("Digite el id a editar");
        id = scan.nextInt();
        for (int e = 0; e < estudiantes.size(); e++) {
            if (estudiantes.get(e).getId()==id) {
            System.out.println("Nombres");
            estudiantes.get(e).setNombres(new Scanner(System.in).nextLine());
            System.out.println("Apellidos:");
            estudiantes.get(e).setApellido(new Scanner(System.in).nextLine());
            System.out.println("Cedula");
            estudiantes.get(e).setCedula(new Scanner(System.in).nextLine());
            System.out.println("Carnet : ");
            estudiantes.get(e).setCarnet(new Scanner(System.in).next());
            System.out.println("telefono:");
            estudiantes.get(e).setTelefono(new Scanner(System.in).next());
         
            }
        }
         return 0;
    }

    @Override
    public boolean delete() throws IOException {
        int id ;
        System.out.println("digite el id a eliminar :");
        id = scan.nextInt();
        for(int i=0 ; i<estudiantes.size(); i++){
            if(estudiantes.get(i).getId()==id){
                estudiantes.remove(i);
            }
        }
         return true;
    }

    @Override
    public List<Estudiante> findAll() throws IOException {
        return estudiantes;
    }
  
}
